package com.example.test.pin;

import javax.validation.Valid;
import javax.xml.stream.events.EntityReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */
@RestController
@RequestMapping("/api/pin")
public class PinController {


    @PostMapping("/validate")
    public ResponseEntity<String> validate(@Valid @RequestBody PinRequest pin) {
        return ResponseEntity.ok("PIN je validní");
    }
}
