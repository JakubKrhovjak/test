package com.example.test.pin;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import lombok.Data;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */

@Data
public class PinRequest {

    @Pattern(regexp = "[0-9]{4}", message = "Nevalidní pin. Pin je čtyř ciferné číslo")
    private String pin;
}
