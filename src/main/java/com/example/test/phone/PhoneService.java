package com.example.test.phone;

import java.util.Set;
import java.util.TreeSet;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */
@Service
public class PhoneService {

    private final static Set<Phone> phones = new TreeSet<>();

    @PostConstruct
    public void init() {
        phones.clear();
        phones.add(new Phone().setNumber("1231").setName("test1"));
        phones.add(new Phone().setNumber("1233").setName("test3"));
        phones.add(new Phone().setNumber("1232").setName("test2"));
    }

    public Set<Phone> fetchPhones() {
        return phones;
    }

    public Phone addPhone(Phone phone) {
        phones.add(phone);
        return phone;
    }

    public Phone updatePhone(Phone phone) {
        removePhone(phone);
        return addPhone(phone);
    }

    public void removePhone(Phone phone) {
        phones.remove(phone);
    }
}
