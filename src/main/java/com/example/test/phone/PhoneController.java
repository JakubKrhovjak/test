package com.example.test.phone;

import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */
@RestController
@RequestMapping("/api/phones")
@RequiredArgsConstructor
public class PhoneController {

    private final PhoneService phoneService;

    @GetMapping
    public Set<Phone> fetchPhones() {
        return phoneService.fetchPhones();
    }

    @PostMapping
    public Phone addPhone(@RequestBody Phone phone) {
       return phoneService.addPhone(phone);
    }

    @PutMapping
    public Phone updatePhone(@RequestBody Phone phone) {
       return phoneService.updatePhone(phone);
    }

    @DeleteMapping("/{name}")
    public void deletePhone(@PathVariable String name) {
        phoneService.removePhone(new Phone().setName(name));
    }

}
