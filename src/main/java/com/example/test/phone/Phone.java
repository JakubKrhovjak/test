package com.example.test.phone;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */

@Data
@Accessors(chain = true)
public class Phone implements Comparable<Phone> {

    private String number;
    private String name;

    @Override
    public int compareTo(Phone phone) {
        return name.compareTo(phone.getName());
    }

}
