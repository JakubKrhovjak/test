package com.example.test.phone;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@SpringBootTest
class PhoneControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private PhoneService phoneService;

    @BeforeEach
    void name() {
        phoneService.init();
    }

    @Test
    void fetchPhones() throws Exception {
        mvc.perform(get("/api/phones"))
            .andExpect(status().is2xxSuccessful())
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].number", is("1231")))
            .andExpect(jsonPath("$[0].name", is("test1")))
            .andExpect(jsonPath("$[1].number", is("1232")))
            .andExpect(jsonPath("$[1].name", is("test2")))
            .andExpect(jsonPath("$[2].number", is("1233")))
            .andExpect(jsonPath("$[2].name", is("test3")));
    }

    @Test
    void addPhone() throws Exception {
        mvc.perform(post("/api/phones")
            .content("{\n"
                + "    \"number\": \"1111\",\n"
                + "    \"name\" : \"test4\"\n"
                + "}")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful())
            .andExpect(jsonPath("$.number", is("1111")))
            .andExpect(jsonPath("$.name", is("test4")));

        Assertions.assertThat(phoneService.fetchPhones())
            .hasSize(4)
            .contains(new Phone().setNumber("1111").setName("test4"));

    }

    @Test
    void updatePhone() throws Exception {
        mvc.perform(put("/api/phones")
                .content("{\n"
                    + "    \"number\": \"2222\",\n"
                    + "    \"name\" : \"test1\"\n"
                    + "}")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful())
            .andExpect(jsonPath("$.number", is("2222")))
            .andExpect(jsonPath("$.name", is("test1")));

        Assertions.assertThat(phoneService.fetchPhones())
            .hasSize(3)
            .contains(new Phone().setNumber("2222").setName("test1"));

    }

    @Test
    void deletePhone() throws Exception {
        mvc.perform(delete("/api/phones/test1"))
            .andExpect(status().is2xxSuccessful());


        Assertions.assertThat(phoneService.fetchPhones())
            .hasSize(2)
            .doesNotContain(new Phone().setNumber("1231").setName("test1"));

    }


}
