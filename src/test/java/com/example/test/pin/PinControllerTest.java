package com.example.test.pin;

import com.example.test.phone.PhoneService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Jakub Krhovják on 5/10/22.
 */
@AutoConfigureMockMvc
@SpringBootTest
public class PinControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void validateOk() throws Exception {
        mvc.perform(post("/api/pin/validate")
                .content("{\n"
                    + "    \"pin\": \"1234\"\n"
                    + "}")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful())
            .andExpect(jsonPath("$", is("PIN je validní")));
    }


    @ParameterizedTest
    @ValueSource(strings = {"ABCD", "12345", "123"})
    void validateFailString(String value) throws Exception {
        mvc.perform(post("/api/pin/validate")
                .content("{\n"
                    + "    \"pin\": \"" + value + "\"\n"
                    + "}")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError());

    }

}
